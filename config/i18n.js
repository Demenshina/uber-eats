import ruLangConfig from '../lang/ru.json'
import enLangConfig from '../lang/en.json'

export default (context) => {
  return {
    fallbackLocale: 'en',
    messages: {
      ru: ruLangConfig,
      en: enLangConfig,
    },
  }
}
